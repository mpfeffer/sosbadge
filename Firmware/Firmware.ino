#include <Adafruit_CC3000.h>
#include <ccspi.h>
#include <utility/socket.h>
#include <SPI.h>

// These are the interrupt and control pins
#define ADAFRUIT_CC3000_IRQ   3  // MUST be an interrupt pin!
// These can be any two pins
#define ADAFRUIT_CC3000_VBAT  5
#define ADAFRUIT_CC3000_CS    10
// Use hardware SPI for the remaining pins
// On an UNO, SCK = 13, MISO = 12, and MOSI = 11
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT,
                                         SPI_CLOCK_DIV2); // you can change this clock speed but DI

uint8_t g_NetworkMAC[6];

#define WLAN_SSID       "SOSBadge"   // cannot be longer than 32 characters!
#define WLAN_PASS       "SOSBadge"
// Security can be WLAN_SEC_UNSEC, WLAN_SEC_WEP, WLAN_SEC_WPA or WLAN_SEC_WPA2
#define WLAN_SECURITY   WLAN_SEC_WPA2

#define UDP_PORT           3769

#define DEBOUNCE_DELAY     50
#define MAGIC_BYTE         0xAD

//Constants
#define BUTTON_PIN_SOS     2    // the number of the sos button pin
#define BUTTON_PIN_CONFIG  4    // the number of the config button pin

#define LED_PIN_STATUS     A2   // pin for the status LED
#define LED_PIN_ERROR      A1   // pin for the error LED

#define CMDID_DISCOVER     0x01
#define CMDID_DISCOVER_ACK 0x02

//Variables
int g_nButtonStateSOS = 0;         // the current reading from the input pin
int g_nLastButtonStateSOS = HIGH;  // the previous reading from the input pin
int g_nButtonStateConfig = 0;         // the current reading from the input pin
int g_nLastButtonStateConfig = HIGH;  // the previous reading from the input pin

// the following variables are long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long g_nLastDebounceTimeSOS = 0;  // the last time the output pin was toggled
long g_nLastDebounceTimeConfig = 0;  // the last time the output pin was toggled

bool g_bServerAddressValid = false;
sockaddr g_ServerAddress;

struct Header
{
  uint8_t m_nMagicByte;
  uint8_t m_nLength;
  uint8_t m_nCmdID;
};

struct Trailer
{
  uint8_t m_nCRC;
};

struct DiscoverCmd
{
  Header  m_Header;
  uint8_t m_NetworkMAC[6];
  Trailer m_Trailer;
};

void setup()
{
  Serial.begin(115200);
  pinMode(BUTTON_PIN_SOS, INPUT);
  pinMode(BUTTON_PIN_CONFIG, INPUT);
  pinMode(LED_PIN_STATUS, OUTPUT);
  pinMode(LED_PIN_ERROR, OUTPUT);
  digitalWrite(BUTTON_PIN_SOS, HIGH);
  digitalWrite(BUTTON_PIN_CONFIG, HIGH);
  
  if(cc3000.begin())
  {
    digitalWrite(LED_PIN_STATUS, LOW);
    digitalWrite(LED_PIN_ERROR, HIGH);
  }
  else
  {
    digitalWrite(LED_PIN_STATUS, HIGH);
    digitalWrite(LED_PIN_ERROR, LOW);
  }
}

uint8_t ComputeCRC(void *pData, size_t nSize)
{
  uint8_t nCRC = 0;
  
  if(NULL != pData)
  {
    if(nSize >= 0)
    {
      for(size_t i = 0; i < nSize; i++)
      {
        nCRC ^= ((uint8_t*)pData)[i];
      }
    }
  }
  
  return nCRC;
}

bool GetIP(uint32_t *pIP, uint32_t *pMask)
{
  uint32_t gateway;
  uint32_t dhcpserv;
  uint32_t dnsserv;
  
  return cc3000.getIPAddress(pIP, pMask, &gateway, &dhcpserv, &dnsserv);
}

void onSOSButtonPressed()
{
  digitalWrite(LED_PIN_STATUS, LOW);
  digitalWrite(LED_PIN_ERROR, LOW);
  
  bool bError = false;
  
  Serial.println(F("BUTTON ok"));
  if(cc3000.getMacAddress(g_NetworkMAC))
  {
    Serial.println(F("MAC ok"));
    
    if(cc3000.deleteProfiles())
    {
      if(cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY))
      {
        Serial.println(F("WLAN ok"));
        while(!cc3000.checkDHCP())
        {
          delay(100); // ToDo: Insert a DHCP timeout!
        }
        
        Serial.println(F("DHCP ok"));
        
        uint8_t ownIP[4];
        uint8_t ownMask[4];
        if(GetIP((uint32_t*)ownIP, (uint32_t*)ownMask))
        {
          Serial.println(F("IP ok"));
          
          //while(false == g_bServerAddressValid)
          {
            Serial.println(F("Socket ok"));
            
            DiscoverCmd discoverCmd;
            memset(&discoverCmd, 0, sizeof(DiscoverCmd));
            discoverCmd.m_Header.m_nMagicByte = MAGIC_BYTE;
            discoverCmd.m_Header.m_nLength = sizeof(DiscoverCmd);
            discoverCmd.m_Header.m_nCmdID = CMDID_DISCOVER;
            memcpy(&(discoverCmd.m_NetworkMAC), g_NetworkMAC, sizeof(g_NetworkMAC));
            discoverCmd.m_Trailer.m_nCRC = ComputeCRC(&discoverCmd, sizeof(DiscoverCmd) - 1);
            
            long nSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
            if(nSocket >= 0)
            {
              struct timeval tv;
              tv.tv_sec = 0;
              tv.tv_usec = 200000;
              if(setsockopt(nSocket, SOL_SOCKET, SOCKOPT_RECV_TIMEOUT, &tv, sizeof(tv)) >= 0)
              {
                sockaddr socketAddress;
                memset(&socketAddress, 0x00, sizeof(socketAddress));
                socketAddress.sa_family = AF_INET;
                socketAddress.sa_data[0] = (UDP_PORT & 0xFF00) >> 8;  // Set the Port Number
                socketAddress.sa_data[1] = (UDP_PORT & 0x00FF);
                socketAddress.sa_data[2] = ownIP[3] | ~ownMask[3];
                socketAddress.sa_data[3] = ownIP[2] | ~ownMask[2];
                socketAddress.sa_data[4] = ownIP[1] | ~ownMask[1];
                socketAddress.sa_data[5] = ownIP[0] | ~ownMask[0];
                
                sendto(nSocket, &discoverCmd, sizeof(DiscoverCmd), 0, &socketAddress, sizeof(socketAddress));
                  
                Serial.println(F("Discover sent"));
                
                bError = false;
              }
            }
          }
          
          //recvfrom(
          
          /*
          long nSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
          if(-1 != nSocket)
          {
            Serial.println(F("Socket ok"));
            
            char strMessage[] = "SOS! Gradma is dying!";
            long nSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        
            sockaddr socketAddress;
            memset(&socketAddress, 0x00, sizeof(socketAddress));
            socketAddress.sa_family = AF_INET;
            socketAddress.sa_data[0] = (UDP_PORT & 0xFF00) >> 8;  // Set the Port Number
            socketAddress.sa_data[1] = (UDP_PORT & 0x00FF);
            socketAddress.sa_data[2] = 192;
            socketAddress.sa_data[3] = 168;
            socketAddress.sa_data[4] = 43;
            socketAddress.sa_data[5] = 1;
        
            sendto(nSocket, strMessage, strlen(strMessage), 0, &socketAddress, sizeof(socketAddress));
              
            Serial.println(F(";-)"));
          }
          */
        }
        
        cc3000.disconnect();
      }
    }
  }
  
  if(true == bError)
  {
    digitalWrite(LED_PIN_STATUS, HIGH);
    digitalWrite(LED_PIN_ERROR, LOW);
  }
  else
  {
    digitalWrite(LED_PIN_STATUS, LOW);
    digitalWrite(LED_PIN_ERROR, HIGH);
  }
}

void onConfigButtonPressed()
{
  for(int i = 0; i < 4; i++)
  {
    digitalWrite(LED_PIN_STATUS, digitalRead(LED_PIN_STATUS) == HIGH ? LOW : HIGH);
    digitalWrite(LED_PIN_ERROR, digitalRead(LED_PIN_ERROR) == HIGH ? LOW : HIGH); 
   
    delay(300);
  }
}

void loop()
{  
  // read the state of the switch into a local variable:
  int nReadingSOS = digitalRead(BUTTON_PIN_SOS);

  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH),  and you've waited
  // long enough since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if(nReadingSOS != g_nLastButtonStateSOS)
  {
    // reset the debouncing timer
    g_nLastDebounceTimeSOS = millis();
  }

  if((millis() - g_nLastDebounceTimeSOS) > DEBOUNCE_DELAY)
  {
    // whatever the reading is at, it's been there for longer
    // than the debounce delay, so take it as the actual current state:

    // if the button state has changed:
    if(nReadingSOS != g_nButtonStateSOS)
    {      
      g_nButtonStateSOS = nReadingSOS;

      if(g_nButtonStateSOS == LOW)
      {
        onSOSButtonPressed();
      }
    }
  }

  // save the reading.  Next time through the loop,
  // it'll be the lastButtonState:
  g_nLastButtonStateSOS = nReadingSOS;
  
  
  // read the state of the switch into a local variable:
  int nReadingConfig = digitalRead(BUTTON_PIN_CONFIG);

  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH),  and you've waited
  // long enough since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if(nReadingConfig != g_nLastButtonStateConfig)
  {
    // reset the debouncing timer
    g_nLastDebounceTimeConfig = millis();
  }

  if((millis() - g_nLastDebounceTimeConfig) > DEBOUNCE_DELAY)
  {
    // whatever the reading is at, it's been there for longer
    // than the debounce delay, so take it as the actual current state:

    // if the button state has changed:
    if(nReadingConfig != g_nButtonStateConfig)
    {      
      g_nButtonStateConfig = nReadingConfig;

      if(g_nButtonStateConfig == LOW)
      {
        onConfigButtonPressed();
      }
    }
  }

  // save the reading.  Next time through the loop,
  // it'll be the lastButtonState:
  g_nLastButtonStateConfig = nReadingConfig;
}
